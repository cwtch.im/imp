use libcwtch::event::{ContactIdentity};

/// defines a locked list of allowed peers and groups the bot may communicate with
/// others will be blocked, and peers listed here will be peered with actively
pub struct AllowListMembers {
    /// list of peers to allow by handle
    pub peers: Vec<ContactIdentity>,
}

impl AllowListMembers {
    /// constructs a new AllowListMembers struct
    pub fn new(peers: Vec<ContactIdentity>) -> Self {
        AllowListMembers {peers: peers}
    }
}

/// How new contacts should be treated
pub enum NewContactPolicy {
    /// Do not react, leave it for the custom event handler
    Ignore,
    /// Block all new contacts
    Block,
    /// Accept all new contacts
    Accept,
    /// AllowList is a list of handles that connections will be allowed from and connected to, and will be accepted
    ///   everything else will be ignored
    AllowList
}

pub enum ContactInteractionPolicy {
    /// Do not react, leave it for the custom event handler
    Ignore,
    /// Accept all messages
    Accept,
    /// Only accept messages from contacts. Pairs strongly with NewContactPolicy of AllowList
    AcceptFromContact,
    /// AllowList is a list of handles that connections will be allowed from and connected to, and will be accepted
    ///   everything else will be ignored
    AllowList
}

pub enum GroupInvitePolicy {
    /// Ignore all group invites
    Ignore,
    /// Accept all group invites
    Accept,
    /// Only accept group invites from contacts. Pairs strongly with NewContactPolicy of AllowList
    AcceptFromContact,
    /// AllowList is a list of handles that group invites will be allowed from and connected to,
    /// and will be accepted. Everything else will be ignored
    AllowList,
}

pub enum GroupInteractionPolicy {
    /// Do not react, leave it for the custom event handler
    Ignore,
    /// Accept all messages
    Accept,
    /// Only accept messages from contacts. Pairs strongly with NewContactPolicy of AllowList
    AcceptFromContact,
    /// AllowList is a list of handles that connections will be allowed from and connected to, and will be accepted
    ///   everything else will be ignored
    AllowList
}

/// Settings for the bot on how it should automatically behave
pub struct Behaviour {
    /// The bot will enable experimental feautres (required for any experiments to be used)
    pub proto_experiments: bool,

    /// The bot will enable the file sharing experiment
    pub proto_experiment_fileshare: bool,

    /// The bot will enable the groups experiment
    pub proto_experiment_groups: bool,

    /// The profile name the bot will share with accepted conversations
    pub profile_name: String,
    /// The profile pic the bot with share with accepted conversations IF the file share exoeriment is enabled
    pub profile_pic_path: Option<String>,

    /// Policy dictacting how the bot should automatically handle ContactCreated events
    pub new_contant_policy: NewContactPolicy,

    /// Policy dictating how the bot should handle messages from p2p contacts
    pub contact_interaction_policy: ContactInteractionPolicy,

    /// Policy dictating how the bot should handle Group Invites
    pub group_invite_policy: GroupInvitePolicy,

    /// Policy dictacting how the bot should respond to @ messages in groups
    pub group_interaction_policy: GroupInteractionPolicy,

    pub allow_list: AllowListMembers,
}

/// intermediary struct for building a Behaviour using builder patern
pub struct BehaviourBuilder {
    behaviour: Behaviour,
}

impl BehaviourBuilder {
    /// Returns a new empty default off for features behaviour builder
    pub fn new() -> Self {
        return BehaviourBuilder {
            behaviour: Behaviour {
                proto_experiments: false,
                proto_experiment_fileshare: false,
                proto_experiment_groups: false,
                new_contant_policy: NewContactPolicy::Ignore,
                contact_interaction_policy: ContactInteractionPolicy::Ignore,
                group_invite_policy: GroupInvitePolicy::Ignore,
                group_interaction_policy: GroupInteractionPolicy::Ignore,
                profile_name: "".to_string(),
                profile_pic_path: None,
                allow_list: AllowListMembers::new(vec!()),
            },
        };
    }

    /// Build the defined behaviours into a Behaviour struct
    pub fn build(self) -> Behaviour {
        self.behaviour
    }

    /// Control if the Behaviour of the bot should include groups (enabling experiments and the group experiment)
    pub fn groups(mut self, val: bool) -> Self {
        self.behaviour.proto_experiment_groups = val;
        self.behaviour.proto_experiments = true;
        self
    }

    /// Control if the Behaviour of the bot should include filesharing (enabling experiments and the filesharing experiment)
    pub fn fileshare(mut self, val: bool) -> Self {
        self.behaviour.proto_experiment_fileshare = val;
        self.behaviour.proto_experiments = true;
        self
    }

    /// Set a profile pic for the bot and enable the filesharing experiment
    pub fn profile_pic_path(mut self, val: String) -> Self {
        self.behaviour.profile_pic_path = Some(val);
        self.behaviour.proto_experiment_fileshare = true;
        self.behaviour.proto_experiments = true;
        self
    }

    /// Set a name for the behaviour
    pub fn name(mut self, val: String) -> Self {
        self.behaviour.profile_name = val;
        self
    }

    /// Set a new contact policy for the behaviour
    pub fn new_contact_policy(mut self, val: NewContactPolicy) -> Self {
        self.behaviour.new_contant_policy = val;
        self
    }

    pub fn contact_interaction_policy(mut self, val: ContactInteractionPolicy) -> Self {
        self.behaviour.contact_interaction_policy = val;
        self
    }

    pub fn group_invite_policy(mut self, val: GroupInvitePolicy) -> Self {
        self.behaviour.group_invite_policy = val;
        self
    }

    pub fn group_interaction_policy(mut self, val: GroupInteractionPolicy) -> Self {
        self.behaviour.group_interaction_policy = val;
        self
    }

    pub fn allow_list(mut self, val: AllowListMembers) -> Self {
        self.behaviour.allow_list = val;
        self
    }
}
