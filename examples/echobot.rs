use std::borrow::BorrowMut;
use std::thread;
use chrono::{DateTime, FixedOffset};
use libcwtch;
use libcwtch::CwtchLib;
use libcwtch::structs::*;
use libcwtch::event::*;
use cwtch_imp::imp;
use cwtch_imp::behaviour::*;
use cwtch_imp::imp::Imp;

const BOT_HOME: &str = "~/.cwtch/bots/echobot";
const PASSWORD: &str = "be gay do crime";
const BOT_NAME: &str = "echobot";

struct Echobot {}

fn main() {
    let behaviour: Behaviour = BehaviourBuilder::new().name(BOT_NAME.to_string()).new_contact_policy(NewContactPolicy::Accept).contact_interaction_policy(ContactInteractionPolicy::Accept).build();
    let event_loop_handle = thread::spawn(move || {
        let mut echobot = Echobot {};
        let mut bot = Imp::spawn(behaviour, PASSWORD.to_string(), BOT_HOME.to_string());
        bot.event_loop::<Echobot>(echobot.borrow_mut());
    });
    event_loop_handle.join().expect("Error running event loop");
}

impl imp::EventHandler for Echobot {
    fn on_new_message_from_contact(&mut self, cwtch: &dyn libcwtch::CwtchLib, behaviour: &mut Behaviour, profile: &Profile, conversation_id: ConversationID, _handle: String, _timestamp_received: DateTime<FixedOffset>, message: MessageWrapper) {
        let response = MessageWrapper {
            o: MessageType::TextMessage,
            d: message.d,
        };
        if let Err(e) = cwtch.send_message(&profile.profile_id, conversation_id, &response) {
            println!("Error sending message: {}", e.to_string());
        }
    }

    fn handle(&mut self, _cwtch: &dyn CwtchLib, _profile_opt: Option<&Profile>, event: &Event) {
        match event {
            Event::NewPeer { profile_id, tag: _, created: _, name, default_picture: _, picture: _, online: _, profile_data: _ } => {
                println!(
                    "\n***** {} at {} *****\n",
                    name, profile_id.as_str()
                );
            }
            _ => (),
        };
    }
}
